package com.zouaoui.fr.tholdi;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONException;
import org.json.JSONObject;

import java.net.CookieHandler;
import java.net.CookieManager;
import java.util.HashMap;
import java.util.Map;

public class ConnectionActivity extends AppCompatActivity {

    EditText login;
    EditText password;
    Button valider;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.connection);

        initialize();
        bindListener();
    }

    /**
     * Initalisation des éléments graphique
     */
    private void initialize() {
        login = (EditText) findViewById(R.id.EditTextLogin);
        password = (EditText) findViewById(R.id.EditTextPassword);
        valider = (Button) findViewById(R.id.ButtonValider);
    }

    /**
     * Mise en place des évenements sur les éléments graphique
     */
    private void bindListener() {
        valider.setOnClickListener(validerListener);
    }

    private View.OnClickListener validerListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            CookieManager cookieManager = new CookieManager();
            CookieHandler.setDefault(cookieManager);
            RequestQueue queue = Volley.newRequestQueue(getApplicationContext());
            StringRequest sr = new StringRequest(Request.Method.POST,"http://sts.ferry-conflans.net:804/wsClient/Connection", new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {
                    JSONObject connected = null;
                    try {
                        connected = new JSONObject(response);
                        boolean state =  connected.getBoolean("connected");
                        if(state){
                            Intent myIntent = new Intent(ConnectionActivity.this, ConsultationActivity.class);
                            ConnectionActivity.this.startActivity(myIntent);
                        }
                    } catch (JSONException e) {
                        Toast.makeText(getApplication(), "login ou mot de passe incorrect", Toast.LENGTH_LONG).show();
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    Toast.makeText(getApplication(), "login ou mot de passe incorrect", Toast.LENGTH_LONG).show();
                }
            }){
                @Override
                protected Map<String,String> getParams(){
                    Map<String,String> params = new HashMap<String, String>();
                    params.put("login", login.getText().toString());
                    params.put("password", password.getText().toString());
                    return params;
                }

                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    Map<String,String> params = new HashMap<String, String>();
                    params.put("Content-Type","application/x-www-form-urlencoded");
                    return params;
                }
            };
            queue.add(sr);
        }
    };

}
