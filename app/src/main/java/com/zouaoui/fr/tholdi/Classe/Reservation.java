package com.zouaoui.fr.tholdi.Classe;

/**
 * Created by zouaoui on 23/04/2017.
 */

public class Reservation {
    private int id;
    private String dateReservation;
    private String datePrevueStockage;
    private int nbJoursDeStockagePrevu;
    private int quantite;
    private String etat;

    /**
     * Constructeur de la classe Reservation
     *
     * @param Id
     * @param DateReservation
     * @param DatePrevueStockage
     * @param NbJoursDeStockagePrevu
     * @param Quantite
     * @param Etat
     */
    public Reservation(int Id, String DateReservation, String DatePrevueStockage, int NbJoursDeStockagePrevu, int Quantite, String Etat) {
        this.id = Id;
        this.dateReservation = DateReservation;
        this.datePrevueStockage = DatePrevueStockage;
        this.nbJoursDeStockagePrevu = NbJoursDeStockagePrevu;
        this.quantite = Quantite;
        this.etat = Etat;
    }

    /**
     * Fonction permettant de récupérer l'identifiant de la réservation
     *
     * @return String
     */
    public String getId() {
        return "Réservation N° " + String.valueOf(id);
    }

    /**
     * Fonction permettant de récupérer la date de reservation
     *
     * @return String
     */
    public String getDateReservation() {
        String date[] = dateReservation.split("T");
        String newDateReservation[] = date[0].split("-");
        return newDateReservation[2] + "/" + newDateReservation[1] + "/" + newDateReservation[0];
    }

    /**
     * Fonction permettant de modifier la date de reservation
     *
     * @param dateReservation
     */
    public void setDateReservation(String dateReservation) {
        this.dateReservation = dateReservation;
    }

    /**
     * Fonction permettant de récupérer la date de stockage prévue
     *
     * @return String
     */
    public String getDatePrevueStockage() {
        String date[] = datePrevueStockage.split("T");
        String newDatePrevueStockage[] = date[0].split("-");
        return newDatePrevueStockage[2] + "/" + newDatePrevueStockage[1] + "/" + newDatePrevueStockage[0];
    }

    /**
     * Fonction permettant de modifier la date de stockage prévue
     *
     * @param datePrevueStockage
     */
    public void setDatePrevueStockage(String datePrevueStockage) {
        this.datePrevueStockage = datePrevueStockage;
    }

    /**
     * Fonction permettant de récupérer la nombre de jours de stockage prévus
     *
     * @return String
     */
    public String  getNbJoursDeStockagePrevu() {
        return  String.valueOf(nbJoursDeStockagePrevu);
    }

    /**
     *Fonction permettant de modifier le nombre de jours de stockage prévus
     *
     * @param nbJoursDeStockagePrevu
     */
    public void setNbJoursDeStockagePrevu(int nbJoursDeStockagePrevu) {
        this.nbJoursDeStockagePrevu = nbJoursDeStockagePrevu;
    }

    /**
     *Fonction permettant de récupérer la quantité de la reservation
     *
     * @return String
     */
    public String getQuantite() {
        return  String.valueOf(quantite);
    }

    /**
     *Fonction permettant de modifier la quantité de la reservation
     *
     * @param quantite
     */
    public void setQuantite(int quantite) {
        this.quantite = quantite;
    }

    /**
     *Fonction permettant de récupérer l'état d'une reservation
     *
     * @return String
     */
    public String getEtat() {
        return etat;
    }

    /**
     *Fonction permettant de modifier l'état d'une réservation
     *
     * @param etat
     */
    public void setEtat(String etat) {
        this.etat = etat;
    }
}
