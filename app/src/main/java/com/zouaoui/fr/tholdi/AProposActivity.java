package com.zouaoui.fr.tholdi;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;

import com.zouaoui.fr.tholdi.R;

/**
 * Created by zouaoui on 23/04/2017.
 */

public class AProposActivity extends AppCompatActivity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.a_propos);
    }
}
