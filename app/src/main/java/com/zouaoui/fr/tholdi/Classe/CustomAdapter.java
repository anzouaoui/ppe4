package com.zouaoui.fr.tholdi.Classe;

import android.app.Activity;
import android.app.LauncherActivity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.TextView;
import android.widget.Toast;

import com.zouaoui.fr.tholdi.R;

import java.util.ArrayList;

/**
 * Created by zouaoui on 23/04/2017.
 */
public class CustomAdapter extends BaseAdapter {

    ArrayList<Reservation> collectionReservation = new ArrayList<Reservation>();
    Context context;

    /**
     * Constructeur de la classe CustomAdapter
     *
     * @param Context
     * @param CollectionReservation
     */
    public CustomAdapter(Context Context, ArrayList<Reservation> CollectionReservation) {
        this.collectionReservation = CollectionReservation;
        this.context = Context;
    }

    /**
     * Retourne le nombre d'objet présent dans notre liste
     *
     * @return int
     */
    @Override
    public int getCount() {
        return collectionReservation.size();
    }

    // retourne un élément de notre liste en fonction de sa position
    @Override
    public Reservation getItem(int position) {
        return collectionReservation.get(position);
    }

    // retourne l'id d'un élément de notre liste en fonction de sa position
    @Override
    public long getItemId(int position) {
        return collectionReservation.indexOf(getItem(position));
    }

    // retourne la vue d'un élément de la liste
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        MyViewHolder mViewHolder = null;

        // au premier appel ConvertView est null, on inflate notre layout
        if (convertView == null) {
            LayoutInflater mInflater = (LayoutInflater) context
                    .getSystemService(Activity.LAYOUT_INFLATER_SERVICE);

            convertView = mInflater.inflate(R.layout.reservation_list, parent, false);

            // nous plaçons dans notre MyViewHolder les vues de notre layout
            mViewHolder = new MyViewHolder();
            mViewHolder.textViewId = (TextView) convertView
                    .findViewById(R.id.TextViewId);
            mViewHolder.textViewDateReservation = (TextView) convertView
                    .findViewById(R.id.TextViewDateReservation);
            mViewHolder.textViewDatePrevueStockage = (TextView) convertView
                    .findViewById(R.id.TextViewDatePrevueStockage);
            mViewHolder.textViewNbJoursDeStockagePrevu = (TextView) convertView
                    .findViewById(R.id.TextViewNbJoursDeStockagePrevu);
            mViewHolder.textViewQuantite = (TextView) convertView
                    .findViewById(R.id.TextViewQuantite);
            mViewHolder.textViewEtat = (TextView) convertView
                    .findViewById(R.id.TextViewEtat);

            // nous attribuons comme tag notre MyViewHolder à convertView
            convertView.setTag(mViewHolder);
        } else {
            // convertView n'est pas null, nous récupérons notre objet MyViewHolder
            // et évitons ainsi de devoir retrouver les vues à chaque appel de getView
            mViewHolder = (MyViewHolder) convertView.getTag();
        }

        // nous récupérons l'item de la liste demandé par getView
        Reservation uneReservation = (Reservation) getItem(position);

        // nous pouvons attribuer à nos vues les valeurs de l'élément de la liste
        mViewHolder.textViewId.setText(String.valueOf(uneReservation.getId()));
        mViewHolder.textViewDateReservation.setText(uneReservation.getDateReservation());
        mViewHolder.textViewDatePrevueStockage.setText(uneReservation.getDatePrevueStockage());
        mViewHolder.textViewNbJoursDeStockagePrevu.setText(String.valueOf(uneReservation.getNbJoursDeStockagePrevu()));
        mViewHolder.textViewQuantite.setText(String.valueOf(uneReservation.getQuantite()));
        mViewHolder.textViewEtat.setText(uneReservation.getEtat());

        // nous retournos la vue de l'item demandé
        return convertView;
    }

    // MyViewHolder va nous permettre de ne pas devoir rechercher
    // les vues à chaque appel de getView, nous gagnons ainsi en performance
    private class MyViewHolder {
        TextView textViewId, textViewDateReservation, textViewDatePrevueStockage, textViewNbJoursDeStockagePrevu, textViewQuantite, textViewEtat;
    }

}