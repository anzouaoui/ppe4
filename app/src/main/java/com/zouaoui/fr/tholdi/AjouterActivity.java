package com.zouaoui.fr.tholdi;

import android.app.DatePickerDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by zouaoui on 23/04/2017.
 */

public class AjouterActivity extends AppCompatActivity {
    EditText editTextDatePrevueStockage;
    Spinner nbJours, quantite;
    DatePickerDialog datePickerDialog;
    Button enregistrer;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.ajouter);
        initialize();
        bindListener();
        addItemSpinnerNbJours();
        addItemSpinnerQuantite();
    }

    private void initialize() {
        editTextDatePrevueStockage = (EditText) findViewById(R.id.EditTextDatePrevueStockage);
        nbJours = (Spinner) findViewById(R.id.SpinnerNbJours);
        quantite = (Spinner) findViewById(R.id.SpinnerQuantite);
        enregistrer = (Button) findViewById(R.id.ButtonEnregistrer);
    }

    private void bindListener() {
        enregistrer.setOnClickListener(enregistrerListener);
    }

    private void addItemSpinnerNbJours() {
        ArrayList<Integer> collectionNbJours = new ArrayList<Integer>();
        for (int i=0 ; i<=50 ; i++) {
            collectionNbJours.add(i);
        }

        ArrayAdapter<Integer>dataAdapter = new ArrayAdapter<Integer>(this, android.R.layout.simple_spinner_item, collectionNbJours);
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        nbJours.setAdapter(dataAdapter);
    }

    private void addItemSpinnerQuantite() {
        ArrayList<Integer> collectionQuantite = new ArrayList<Integer>();
        for (int i=0 ; i<=50 ; i++) {
            collectionQuantite.add(i);
        }

        ArrayAdapter<Integer> dataAdapter = new ArrayAdapter<Integer>(this, android.R.layout.simple_spinner_item, collectionQuantite);
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        quantite.setAdapter(dataAdapter);
    }

    private View.OnClickListener enregistrerListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            RequestQueue queue = Volley.newRequestQueue(getApplicationContext());
            StringRequest sr = new StringRequest(Request.Method.POST,"http://sts.ferry-conflans.net:804/wsClient/Reservation", new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {
                    JSONObject resultat = null;
                    try {
                        resultat = new JSONObject(response);
                        String state = resultat.getString("resultat");
                        Intent myIntent = new Intent(AjouterActivity.this, ConsultationActivity.class);
                        AjouterActivity.this.startActivity(myIntent);
                        Toast.makeText(getApplicationContext(), state, Toast.LENGTH_LONG);
                    } catch (JSONException e) {
                        Toast.makeText(getApplication(),"Erreur JSON: " + e.getMessage(), Toast.LENGTH_LONG).show();
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    Toast.makeText(getApplication(), "Erreur volley: + " + error.getMessage(), Toast.LENGTH_LONG).show();
                }
            }){
                @Override
                protected Map<String,String> getParams(){
                    Map<String,String> params = new HashMap<String, String>();
                    String date[] =  editTextDatePrevueStockage.getText().toString().split("/");
                    String newDatePrevueSotckage = date[2] + "/" + date[1] + "/" + date[0];
                    params.put("dateDebut",newDatePrevueSotckage);
                    params.put("nbJours", nbJours.getSelectedItem().toString());
                    params.put("quantite", quantite.getSelectedItem().toString());
                    return params;
                }

                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    Map<String,String> params = new HashMap<String, String>();
                    params.put("Content-Type","application/x-www-form-urlencoded");
                    return params;
                }
            };
            queue.add(sr);
        }
    };
}
