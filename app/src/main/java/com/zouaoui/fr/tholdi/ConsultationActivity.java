package com.zouaoui.fr.tholdi;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.HttpHeaderParser;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.JsonRequest;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.zouaoui.fr.tholdi.Classe.CustomAdapter;
import com.zouaoui.fr.tholdi.Classe.Reservation;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by zouaoui on 01/04/2017.
 */

public class ConsultationActivity extends AppCompatActivity {
    TextView numReservation, dateReservation, datePrevuStockage, nbJoursStockagePrevu, quantite, etat;
    LinearLayout listeReservation;
    ListView collectionReservation;
    int[] collectionId;
    String[] collectionDateReservation;
    String[] collectionDatePrevueStockage;
    int[] collectionNbJoursDeStockagePrevu;
    int[] collectionQuantite;
    String[] collectionEtat;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.consultation);
        initialize();


        RequestQueue queue = Volley.newRequestQueue(getApplicationContext());
        JsonArrayRequest sr = new JsonArrayRequest(Request.Method.GET, "http://sts.ferry-conflans.net:804/wsClient/Reservation", null, new Response.Listener<JSONArray>() {
            @Override
            public void onResponse(JSONArray response) {
                try {
                    ArrayList<Reservation> listeReservation = new ArrayList<Reservation>();
                    Integer Id;
                    String DateReservation;
                    String DatePrevueStockage;
                    Integer NbJoursDeStockagePrevu;
                    Integer Quantite;
                    String Etat;
                    for(int i=0 ; i<response.length(); i++) {
                        JSONArray reservationCourante = (JSONArray) response.get(i);
                        collectionId = new int[reservationCourante.length()];
                        collectionDatePrevueStockage = new String[reservationCourante.length()];
                        collectionDateReservation = new String[reservationCourante.length()];
                        collectionNbJoursDeStockagePrevu = new int[reservationCourante.length()];
                        collectionQuantite = new int[reservationCourante.length()];
                        collectionEtat = new String[reservationCourante.length()];
                        for(int j=0 ; j<reservationCourante.length() ; j++) {
                            JSONObject uneReservation = reservationCourante.getJSONObject(j);
                            Id = uneReservation.getInt("Id");
                            collectionId[j] = Id;
                            DateReservation = uneReservation.getString("Datereservation");
                            collectionDateReservation[j] = DateReservation;
                            DatePrevueStockage = uneReservation.getString("Dateprevuestockage");
                            collectionDatePrevueStockage[j] = DatePrevueStockage;
                            NbJoursDeStockagePrevu = uneReservation.getInt("Nbjoursdestockageprevu");
                            collectionNbJoursDeStockagePrevu[j] = NbJoursDeStockagePrevu;
                            Quantite = uneReservation.getInt("Quantite");
                            collectionQuantite[j] = Quantite;
                            Etat = uneReservation.getString("Etat");
                            if(Etat == "encours") {
                                int taille = Etat.length();
                                for (int l=0 ; l<taille ; l++) {
                                    String newEtat = Etat.substring(0, 2) + " " + Etat.substring(2);
                                    Etat = newEtat;
                                }
                            }
                            collectionEtat[j] = Etat;
                        }
                        for(int k=0 ; k<collectionId.length ; k++) {
                            listeReservation.add(new Reservation(collectionId[k],collectionDateReservation[k], collectionDatePrevueStockage[k], collectionNbJoursDeStockagePrevu[k], collectionQuantite[k], collectionEtat[k]));
                        }
                    }
                    CustomAdapter adapter = new CustomAdapter(getApplicationContext(), listeReservation);
                    collectionReservation.setAdapter(adapter);

                }catch (JSONException e) {
                    e.printStackTrace();
                    Toast.makeText(getApplicationContext(), "Error: " + e.getMessage(), Toast.LENGTH_LONG).show();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(getApplicationContext(), "Error: " + error.getMessage(), Toast.LENGTH_LONG).show();
            }
        });
        queue.add(sr);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.MenuAjout:
                Intent appelAjout = new Intent(ConsultationActivity.this, AjouterActivity.class);
                ConsultationActivity.this.startActivity(appelAjout);
            case R.id.MenuAPropos:
                Intent appelAPropos = new Intent(ConsultationActivity.this, AProposActivity.class);
                ConsultationActivity.this.startActivity(appelAPropos);
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void initialize() {
        collectionReservation = (ListView) findViewById(R.id.ListViewReservations);
        numReservation = (TextView) findViewById(R.id.TextViewId);
        dateReservation = (TextView) findViewById(R.id.TextViewDateReservation);
        datePrevuStockage = (TextView) findViewById(R.id.TextViewDatePrevueStockage);
        nbJoursStockagePrevu = (TextView) findViewById(R.id.TextViewNbJoursDeStockagePrevu);
        quantite = (TextView) findViewById(R.id.TextViewQuantite);
        etat = (TextView) findViewById(R.id.TextViewEtat);
    }
}